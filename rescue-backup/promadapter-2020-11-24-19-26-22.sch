EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ikrlib
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74S571 U2
U 1 1 5AEB94BA
P 6975 3850
F 0 "U2" H 6975 4075 60  0000 C CNN
F 1 "74S571" H 6975 3950 60  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm" H 6975 3950 60  0001 C CNN
F 3 "" H 6975 3950 60  0000 C CNN
	1    6975 3850
	-1   0    0    1   
$EndComp
$Comp
L 2716 U1
U 1 1 5AEB9599
P 5475 4350
F 0 "U1" H 5625 4150 50  0000 C CNN
F 1 "2716" H 5625 3950 50  0000 C CNN
F 2 "Housings_DIP:DIP-24_W15.24mm" H 5475 4350 50  0001 C CNN
F 3 "" H 5475 4350 50  0000 C CNN
	1    5475 4350
	-1   0    0    -1  
$EndComp
$Comp
L VCC #PWR01
U 1 1 5AEB981E
P 6275 4850
F 0 "#PWR01" H 6275 4700 50  0001 C CNN
F 1 "VCC" H 6275 5000 50  0000 C CNN
F 2 "" H 6275 4850 50  0000 C CNN
F 3 "" H 6275 4850 50  0000 C CNN
	1    6275 4850
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P1
U 1 1 5AEB9A4E
P 4200 3800
F 0 "P1" H 4200 4050 50  0000 C CNN
F 1 "CONN_01X04" V 4300 3800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 4200 3800 50  0001 C CNN
F 3 "" H 4200 3800 50  0000 C CNN
	1    4200 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6175 3650 6425 3650
Wire Wire Line
	6425 3750 6175 3750
Wire Wire Line
	6175 3850 6425 3850
Wire Wire Line
	6425 3950 6175 3950
Wire Wire Line
	6175 4050 6425 4050
Wire Wire Line
	6425 4150 6175 4150
Wire Wire Line
	6175 4250 6425 4250
Wire Wire Line
	6425 4350 6175 4350
Wire Wire Line
	6175 4450 6425 4450
Wire Wire Line
	6425 3300 6350 3300
Wire Wire Line
	6350 3300 6350 5050
Wire Wire Line
	6350 5050 6175 5050
Wire Wire Line
	6175 4950 6350 4950
Connection ~ 6350 4950
Wire Wire Line
	6175 4850 6750 4850
Wire Wire Line
	4475 2925 4475 4350
Wire Wire Line
	4475 2925 7850 2925
Wire Wire Line
	7850 2925 7850 3700
Wire Wire Line
	7850 3700 7575 3700
Wire Wire Line
	4550 3000 4550 4250
Wire Wire Line
	4550 3000 7775 3000
Wire Wire Line
	7775 3000 7775 3800
Wire Wire Line
	7775 3800 7575 3800
Wire Wire Line
	4625 3075 4625 4150
Wire Wire Line
	4625 3075 7700 3075
Wire Wire Line
	7700 3075 7700 3900
Wire Wire Line
	7700 3900 7575 3900
Wire Wire Line
	7575 4000 7625 4000
Wire Wire Line
	4700 3150 7625 3150
Wire Wire Line
	7625 3150 7625 4000
Wire Wire Line
	4700 3150 4700 4050
Wire Wire Line
	4700 4050 4775 4050
Wire Wire Line
	4625 4150 4775 4150
Wire Wire Line
	4550 4250 4775 4250
Wire Wire Line
	4475 4350 4775 4350
Wire Wire Line
	4400 3650 4775 3650
Wire Wire Line
	4775 3750 4400 3750
Wire Wire Line
	4400 3850 4775 3850
Wire Wire Line
	4775 3950 4400 3950
$Comp
L CONN_02X03 P2
U 1 1 5AEB9DD4
P 7050 4875
F 0 "P2" H 7050 5075 50  0000 C CNN
F 1 "CONN_02X03" H 7050 4675 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 7050 3675 50  0001 C CNN
F 3 "" H 7050 3675 50  0000 C CNN
	1    7050 4875
	0    1    1    0   
$EndComp
Wire Wire Line
	7050 4625 7050 4550
Wire Wire Line
	7050 4550 6175 4550
Wire Wire Line
	6175 4650 6675 4650
Wire Wire Line
	6675 4650 6675 5250
Wire Wire Line
	6675 5250 7050 5250
Wire Wire Line
	7050 5250 7050 5125
Wire Wire Line
	6950 5125 6750 5125
Wire Wire Line
	6750 5125 6750 4625
Wire Wire Line
	6750 4625 6950 4625
Connection ~ 6750 4850
Connection ~ 6275 4850
$Comp
L GND #PWR02
U 1 1 5AEB9F09
P 7150 5175
F 0 "#PWR02" H 7150 4925 50  0001 C CNN
F 1 "GND" H 7150 5025 50  0000 C CNN
F 2 "" H 7150 5175 50  0000 C CNN
F 3 "" H 7150 5175 50  0000 C CNN
	1    7150 5175
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 5175 7150 5125
Wire Wire Line
	7150 5150 7300 5150
Wire Wire Line
	7300 5150 7300 4625
Wire Wire Line
	7300 4625 7150 4625
Connection ~ 7150 5150
$EndSCHEMATC
